package com.example.bar_tender.controllers;


import com.example.bar_tender.models.ServiceRequest;
import com.example.bar_tender.services.ServiceRequestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ListDataController {

    final ServiceRequestService serviceRequestService;

    public ListDataController(ServiceRequestService serviceRequestService){
        this.serviceRequestService = serviceRequestService;
    }

    @GetMapping("/requests")
    public ResponseEntity<List<ServiceRequest>> getAllRequests(){
        List<ServiceRequest> serviceRequests = serviceRequestService.getAllRequests();
        return new ResponseEntity<>(serviceRequests, HttpStatus.OK);
    }

    @GetMapping("/requests/{customerId}")
    public ResponseEntity<List<ServiceRequest>> getRequestsBy(@PathVariable("customerId") int customerId){
        List<ServiceRequest> serviceRequests = serviceRequestService.getRequestsBy(customerId);
        return new ResponseEntity<>(serviceRequests, HttpStatus.OK);
    }

}
