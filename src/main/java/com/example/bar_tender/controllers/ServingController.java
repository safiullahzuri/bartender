package com.example.bar_tender.controllers;


import com.example.bar_tender.models.ServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@RestController
public class ServingController {

    Logger logger = LoggerFactory.getLogger(ServingController.class);

    static int drinks = 0;
    static int beers = 0;

    @Value("${drink.preparation.time}")
    private static int drinkPreparationTime;

    static ExecutorService beerServiceExecutor = Executors.newFixedThreadPool(2);
    static ExecutorService drinkServiceExecutor = Executors.newSingleThreadExecutor();

    @PostMapping("/serve")
    public ResponseEntity<?> serveDrink(@RequestBody ServiceRequest serviceRequest){
        logger.info("Receiving serve request => "+serviceRequest.toString());
        if (isAvailableToServe(serviceRequest.getDrinkType())){

            increaseDrinksServed(serviceRequest.getDrinkType());

            if (serviceRequest.getDrinkType().equalsIgnoreCase("beer")){
                beerServiceExecutor.execute(new ServiceThread(serviceRequest));
            }else if (serviceRequest.getDrinkType().equalsIgnoreCase("drink")){
                drinkServiceExecutor.execute(new ServiceThread(serviceRequest));
            }
            return new ResponseEntity<>("drink of type "+serviceRequest.getDrinkType()+" served.", HttpStatus.OK);
        }else {
            return new ResponseEntity<>("could not serve drink of type "+serviceRequest.getDrinkType(), HttpStatus.TOO_MANY_REQUESTS);
        }

    }



    static class ServiceThread extends Thread{
        ServiceRequest serviceRequest;
        public ServiceThread(ServiceRequest serviceRequest){
            this.serviceRequest = serviceRequest;
        }

        @Override
        public void run() {
            String responseString = "Your drink has been prepared.";
            System.out.println(responseString);

            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            decreaseDrinksServed(serviceRequest.getDrinkType());
        }
    }


    private static boolean isAvailableToServe(String drinkType){
        if (drinkType.equalsIgnoreCase("beer")){
            return drinks == 0 && beers < 2;
        }else if (drinkType.equalsIgnoreCase("drink")){
            return beers == 0 && drinks == 0;
        }
        return false;
    }

    private static void increaseDrinksServed(String drinkType){
        if (drinkType.equalsIgnoreCase("beer")){
            ++beers;
        }else if (drinkType.equalsIgnoreCase("drink")){
            ++drinks;
        }
    }

    private static void decreaseDrinksServed(String drinkType){
        if (drinkType.equalsIgnoreCase("beer")){
            --beers;
        }else if (drinkType.equalsIgnoreCase("drink")){
            --drinks;
        }
    }
}
