package com.example.bar_tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BarTenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarTenderApplication.class, args);
	}

}
