package com.example.bar_tender.services;


import com.example.bar_tender.models.ServiceRequest;
import com.example.bar_tender.repos.ServiceRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ServiceRequestService {

    @Autowired
    private ServiceRequestRepository serviceRequestRepository;


    public void saveRequest(ServiceRequest serviceRequest){
        serviceRequestRepository.save(serviceRequest);
    }

    public List<ServiceRequest> getAllRequests(){
        return serviceRequestRepository.findAll();
    }

    public List<ServiceRequest> getRequestsBy(int customerId){
        return this.getAllRequests().stream().filter(e -> e.getCustomerId() == customerId).collect(Collectors.toList());
    }



}
